Command-line Interface
======================
Remt is a command-line application. This, combined with a decent shell,
gives an user features, which are rare in GUI programs

- easily execute or find previous commands
- combine multiple commands to unlock more features
- include Remt in scripts to automate tasks

Basic Usage
-----------
Use ``remt`` command to execute note-taking workflow tasks on reMarkable
tablet device. The main Remt commands and parameters are documented in
this manual, but use ``--help`` option to refer to the full list of
commands::

    $ remt --help
    usage: remt [-h] {ls,mkdir,export,import,index} ...

    remt 0.10.0 - note-taking support tool for reMarkable tablet

    options:
      -h, --help            show this help message and exit

    subcommands:
      {ls,mkdir,export,import,index}
        ls                  list files on the tablet
        mkdir               create a directory on the tablet
        export              export a notebook or an annotated PDF file from the tablet
        import              import a number of PDF files onto the tablet
        index               create index of PDF file annotations

The option can show help for a specific command as well::

    $ remt ls --help
    usage: remt ls [-h] [-l] [-R] [path]

    positional arguments:
      path        Starting path (%i for interactive selection)

    optional arguments:
      -h, --help  show this help message and exit
      -l          Use long listing format
      -R          List subdirectories recursively

Interactive Selection
---------------------
It is hard to remember the structure of directories and files on a device.
For that reason, many shells support previous command search and path
expansion.

Remt enables user to find and select files and directories on a
reMarkable tablet in an interactive way, which simplifies execution of
commands.

For example, type the following command to list files on reMarkable tablet,
find and select a file, and save the file in current directory::

    $ remt export %i .

The help output of Remt commands indicates, which positional arguments
accept `%i` for interactive selection. An argument points to a file
or a directory on reMarkable tablet.

More examples of Remt command execution can be found in :ref:`workflow`
section.

Combining Commands
------------------
Combine Remt commands with other command-line utilities to unlock more
features.

For example, to count number of files and directories on reMarkable tablet,
send output of ``remt ls`` command to ``wc`` Unix-like operating system
utility::

    $ remt ls -l -R | wc -l
    282

To count number of files only, filter out directories with ``grep``
command-line utility by constructing the pipeline as follows::

    $ remt ls -l -R | grep -v '^d' | wc -l
    227

.. vim: sw=4:et:ai
