Installation
============
Dependencies
------------
Remt requires Python 3.9 or later, up-to-date firmware on reMarkable
tablet, and many third party libraries.

It can work with older versions of reMarkable tablet firmware, but please
use version 2.11 or later.

The list of required libraries can be found in `Setuptools configuration
file <https://gitlab.com/wrobell/remt/-/blob/master/setup.cfg>`_ located
in Remt source code repository. Most of the dependencies are downloaded
during installation without a problem (see next section). However, it is
the best, for libraries like Pycairo, PyGObject and Poppler, to use
packages provided by an operating system or Python distribution.

The table below shows list of recommended packages to be installed for an
operating system or Python distribution before installing Remt.

+---------------+----------------------------+
| Distribution  |        Packages            |
+===============+============================+
| ArchLinux     | ``gobject-introspection``, |
|               | ``python-cairo``,          |
|               | ``poppler``                |
+---------------+----------------------------+
| Ubuntu/Debian | ``gir1.2-poppler-0.18``,   |
|               | ``python3-gi-cairo``       |
+---------------+----------------------------+
| Fedora        | ``pygobject3``,            |
|               | ``python3-gobject``,       |
|               | ``poppler``,               |
|               | ``cairo-gobject``          |
+---------------+----------------------------+
| Anaconda      | ``pygobject``,             |
|               | ``poppler``,               |
|               | ``cairo``                  |
+---------------+----------------------------+

.. note:: Please `create a ticket
   <https://gitlab.com/wrobell/remt/-/issues>`_ if the table contains an
   error or to add a new distribution.

Installation and Configuration
------------------------------
Install or upgrade Remt with command::

    $ pip install -U remt

The command downloads Remt software and all of the dependencies.

Create configuration file `~/.config/remt.ini` with contents::

    [connection]
    host=10.11.99.1
    user=root
    password=<reMarkable tablet password>

The password can be found on reMarkable tablet near the bottom of copyright
and licenses statement:

.. code-block:: none

   Menu -> Settings -> Help -> Copyrights and licenses

Check installation by listing files on the reMarkable tablet::

    $ remt ls

.. vim: sw=4:et:ai
