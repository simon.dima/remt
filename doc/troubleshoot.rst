Troubleshooting
===============
Remt connects to reMarkable tablet using SSH File Transfer Protocol.
This section provides some troubleshooting advice in case of SSH key
problems or network issues.

SSH Key Problems
----------------
The following error, when executing a Remt command, usually means there
is an issue with SSH key::

    [Errno 104] Connection reset by peer

Try to connect to reMarkable tablet using a SSH client. If there are any
warnings about SSH key, then correct the issues as instructed by SSH
client.

SSH key might need to be regenerated with key length longer than 512 if the
error below is displayed::

    ValueError: p must be exactly 1024, 2048, or 3072 bits long

Invalid Password
----------------
If SSH key is not used, then password to reMarkable tablet needs to be set
correctly in the `~/.config/remt.ini` configuration file.

Data Transfer Issues
--------------------
When data transfer to reMarkable tablet is interrupted randomly

1. There might be problem with network if Wi-Fi is used.
2. USB cable might be of bad quality or broken if USB connection is used.

To ensure connection quality, try to transfer few megabytes file to
reMarkable tablet. If that fails, follow the usual advice on network
quality issues.

When using connection over USB cable, check if there are any USB errors
reported by personal computer's operating system. For example, on Linux
`dmesg` command can be used in such situation. Try to replace the cable if
there are any errors or warnings.

Connection Refused
------------------
The following error can be reported, when using reMarkable tablet renderer
with export command::

    urllib.error.URLError: <urlopen error [Errno 111] Connection refused>

Ensure that reMarkable tablet is connected to personal computer using good
quality USB cable. Check that web interface is enabled on the tablet.

.. vim: sw=4:et:ai
