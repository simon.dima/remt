Changelog
=========
2021-12-23, ver. 0.10.1
-----------------------
- fix installation issues

2021-12-22, ver. 0.10.0
-----------------------
- Remt is a note-taking support tool, now
- support new features introduced in reMarkable firmware 2.10.x and 2.11.x

  - annotated text with highlighter; the information about annotated text
    is stored in separate files and required new implementation
  - green and pink highlighter colors
  - red and blue colors for other tools

- remove support for raster brushes
- Remt documentation is written

.. vim: sw=4:et:ai
