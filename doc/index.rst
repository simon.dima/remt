Remt - Note-taking Support Tool
===============================

.. figure:: rm-cropped.*
   :align: right
   :width: 120
   :alt: Drawing exported with Remt

Remt project is note-taking support tool for reMarkable tablet

    https://remarkable.com/

The application allows to import PDF documents, export PDF documents and
notebooks, and list PDF text annotations.

This documentation can be downloaded in `PDF format <remt-0.10.1.pdf>`_.

Remt is *not* an official project of the reMarkable company.

Remt project is licensed under terms of `GPL license, version 3
<https://www.gnu.org/licenses/gpl-3.0.en.html>`_. As stated in the license,
there is no warranty, so any usage is on your own risk.

.. toctree::
   :caption: Table of Contents
   :maxdepth: 2

   intro
   install
   cmd
   workflow
   troubleshoot
   links
   changelog

.. vim: sw=4:et:ai
