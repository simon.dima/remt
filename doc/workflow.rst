.. _workflow:

Workflow
========
Remt supports a note-taking workflow with a combination of the following
features

- import of PDF files to reMarkable tablet
- export of annotated PDF files and notebooks from the device
- making an index of a PDF file annotations 

File Import
-----------
Import a PDF file to reMarkable tablet with ``remt import`` command.

For example, to import `a-paper.pdf` document::

    $ remt import a-paper.pdf %i

The ``%i`` argument tells Remt to show list of available directories on
reMarkable tablet and allows to select the destination directory in an
interactive way.

To upload the PDF file to a known destination directory::

    $ remt import a-paper.pdf articles

To import multiple files, list the files one by one, or by using wildcards
supported by users' shell::

    $ remt import a.pdf b.pdf ?-paper.pdf a01-*.pdf articles

.. note::
   After PDF file import, reMarkable tablet needs to be restarted to
   refresh list of files on the device.

   Remt does not know how to notify reMarkable tablet, that a file is
   uploaded. At the moment, it is recommended to upload a file or multiple
   files and then restart reMarkable tablet.

File Export
-----------
Export a PDF file or a notebook from reMarkable tablet with ``remt export``
command.

The easiest way to export a PDF file from the device is::

    $ remt export %i .

The command above allows to interactively select file stored on the tablet
and saves exported file in current directory.

Alternatively, if a name of a document is known::

    $ remt export articles/a-paper.pdf .

Original Renderer 
^^^^^^^^^^^^^^^^^
Remt uses its own, custom renderer for notebooks and PDF file annotations
by default. Alternatively, select reMarkable tablet's renderer with ``-r``
option, for example::

    $ remt export -r remarkable %i .

To use the original renderer, connect reMarkable tablet to a computer via
USB cable and enable USB web interface. More information about the web
interface can be found at

- https://blog.remarkable.com/how-to-transfer-files-to-your-remarkable-without-wi-fi-d53956bd9869
- https://remarkablewiki.com/tech/webinterface

PDF Annotations Indexer
-----------------------
Create index of annotations of a PDF document with ``remt index`` option.

For example, to create index of annotations for a document and save it in
HTML file::

    $ remt index %i | rst2html - > doc-index.html

When creating index of PDF file, Remt recognizes highlighter annotations,
and tries to merge overlapping horizontal highlights of text.

Use reMarkable tablet `text highlighting
<https://support.remarkable.com/hc/en-us/articles/360002661597-Pen-pencil-brush-highlighter-Writing-tools>`_
features for the best experience.

.. vim: sw=4:et:ai
